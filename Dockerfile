FROM caddy:2.4.3-builder as builder

RUN xcaddy build \
	--with github.com/caddy-dns/cloudflare \
	--with github.com/kirsch33/realip

FROM caddy:2.4.3
COPY --from=builder /usr/bin/caddy /usr/bin/caddy
